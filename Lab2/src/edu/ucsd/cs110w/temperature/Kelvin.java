/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author Dylan
 * 
 */
public class Kelvin extends Temperature {

	public Kelvin(float t) {
		super(t);
	}

	public String toString() {
		String s = Float.toString(this.getValue()) + " K";
		return s;
	}

	public Temperature toCelsius() {
		float value = (this.getValue() - 273.15f);
		return new Celsius(value);
	}

	public Temperature toFahrenheit() {
		float value = (((getValue() - 273.15f ) * 1.8f) + 32.0f);
		return new Fahrenheit(value);
	}
	
	public Temperature toKelvin() {
		return this;
	}
}
