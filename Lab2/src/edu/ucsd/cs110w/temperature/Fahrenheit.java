/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author Rubbrchikin
 *
 */
public class Fahrenheit extends Temperature {

	public Fahrenheit(float t) 
	 { 
	 super(t); 
	 } 
	 public String toString() 
	 { 
		 String s = Float.toString(this.getValue()) + " F";
		 return s;
	 }
	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toCelsius()
	 */
	public Temperature toCelsius() {
		float value = ((getValue() - 32.0f) * 0.555556f);
		return new Celsius(value);
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toFahrenheit()
	 */
	public Temperature toFahrenheit() {
		return this;
	}
	
	public Temperature toKelvin() {
		float value = (((getValue() - 32.0f ) * 0.55556f) + 273.15f);
		return new Kelvin(value);
	}

}
