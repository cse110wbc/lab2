/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author Rubbrchikin
 *
 */
public class Celsius extends Temperature {

	public Celsius(float t)
	 {
		super(t); 
	 } 
	 public String toString()
	 { 
		 String s = Float.toString(this.getValue()) + " C";
		 return s;
	 }
	 
	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toCelsius()
	 */
	public Temperature toCelsius() {		
		return this;
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toFahrenheit()
	 */
	public Temperature toFahrenheit() {
		float value = (this.getValue() * 1.8f + 32.0f);
		return new Fahrenheit(value);
	}
	
	public Temperature toKelvin() {
		float value = (this.getValue() + 273.15f);
		return new Kelvin(value);
	}

}
